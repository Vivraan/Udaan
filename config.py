import os
from pathlib import PurePath
from typing import List, Dict

path = PurePath(__file__).parent
db_path: PurePath = path / 'project.db'
migrate_repo_path: PurePath = path / 'db_repository'


class FlaskConfig:
    SECRET_KEY: str = (
        os.environ.get('SECRET_KEY')
        or 'dev'
    )

    SQLALCHEMY_DATABASE_URI: str = (
        os.environ.get('SQLALCHEMY_DATABASE_URI')
        or f'sqlite:///{db_path}'
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False



def use_render_as_batch() -> bool:
    """Returns true if SQLite is used in FlaskConfig."""
    return 'sqlite' in FlaskConfig.SQLALCHEMY_DATABASE_URI


# CRUCIAL for SQLite databases since batch operations fail on unnamed constraints
DB_NAMING_CONVENTION: Dict[str, str] = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}
