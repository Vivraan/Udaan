from datetime import datetime
from enum import Enum

from flask import request

from app import db, ma


class Asset(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)
    description = db.Column(db.String)
    tasks = db.relationship('Task', backref='asset', lazy='dynamic')

    def __repr__(self):
        return f'<Asset {self.name}>'


class TaskFrequency(Enum):
    HOURLY = 'hourly'
    DAILY = 'daily'
    WEEKLY = 'weekly'
    MONTHLY = 'monthly'
    YEARLY = 'yearly'


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)
    description = db.Column(db.String)
    frequency = db.Column(db.Enum(TaskFrequency))
    start_datetime = db.Column(db.DateTime, default=datetime.utcnow)
    end_datetime = db.Column(db.DateTime, default=datetime.utcnow)
    asset_id = db.Column(db.Integer, db.ForeignKey('asset.id'))
    worker_id = db.Column(db.Integer, db.ForeignKey('worker.id'))

    def __repr__(self):
        return f'<Task {self.name}>'


class Worker(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True)
    tasks = db.relationship('Task', backref='tasks', lazy='dynamic')

    def __repr__(self):
        return f'<Worker {self.name}>'


class AssetSerializer(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'description')


asset = AssetSerializer(strict=True)
assets = AssetSerializer(strict=True, many=True)


class TaskSerializer(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'description', 'frequency', 'start_dt', 'end_dt')

task = TaskSerializer(strict=True)
tasks = TaskSerializer(strict=True, many=True)


class WorkerSerializer(ma.Schema):
    class Meta:
        fields = ('id', 'name')

worker = WorkerSerializer(strict=True)
workers = WorkerSerializer(strict=True, many=True)
