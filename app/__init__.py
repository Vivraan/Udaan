from datetime import datetime

from flask import Flask, abort, jsonify, request
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import MetaData

from config import *

app = Flask(__name__)
app.config.from_object(FlaskConfig)
db = SQLAlchemy(
    app,
    metadata=MetaData(naming_convention=DB_NAMING_CONVENTION)
)
migrate = Migrate(app, db, render_as_batch=use_render_as_batch())
ma = Marshmallow(app)

if app and db and migrate:
    from app import models
    from app import endpoints

    app.register_blueprint(endpoints.bp)
        
    @app.route('/')
    @app.route('/spec')
    def spec():
        return 'Just an empty homepage. Spec as per requirements.'
        
