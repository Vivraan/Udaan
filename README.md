# Housekeeping API

## WARNING

This project is not backwards compatible with Python 2 or <3.6.

## Installation

1. (Optional) Create a virtual environment.
1. Install the modules listed in `requirements.txt`:

    ```shell
    pip install -r requirements.txt
    ```

1. Set up these environment variables**\***:

    ```shell
    set FLASK_APP="app"
    set FLASK_ENV="development"
    set FLASK_DEBUG="0" # or 1, if it's to be debugged
    set SECRET_KEY="<your-secret-key>"
    ```

1. Initialise the database and set up migrations:

    ```shell
    flask db init
    flask db migrate -m "Setup tables"
    flask db upgrade
    ```

1. Run the app:

    ```shell
    flask run
    ```
