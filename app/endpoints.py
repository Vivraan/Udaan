from flask import Blueprint, request

bp = Blueprint('endpoints', __name__, url_prefix='/api')

@bp.errorhandler(422)
def invalidInput(error):
    return jsonify({'message': error.description})


@bp.route('/add-asset', methods=['POST'])
def add_asset():
    name = request.json['name']
    description = request.json['description']
    new_asset = Asset(
        name=name,
        description=description
    )
    db.session.add(new_asset)
    db.session.commit()
    return jsonify({'status': 'Success.'})


@bp.route('/add-task', methods=['POST'])
def add_task():
    new_task = Task(
        name=request.json['name'],
        description=request.json['description'],
        frequency=TaskFrequency(
            request.json['frequency'])
    )
    db.session.add(new_task)
    db.session.commit()
    return jsonify({'status': 'Success.'})


@bp.route('/add-worker', methods=['POST'])
def add_worker():
    new_task = Task(
        name=request.json['name'],
    )
    db.session.add(new_task)
    db.session.commit()
    return jsonify({'status': 'Success.'})


@bp.route('/assets/all', methods=['GET'])
def get_all_assets():
    all_assets = Asset.query.all()
    result = assets.dump(all_assets)
    return jsonify(result.data)


@bp.route('/allocate-task', methods=['POST'])
def allocate_task():
    asset_id = request.json['assetId']
    task_id = request.json['taskId']
    worker_id = request.json['workerId']

    start_datetime = datetime.utcnow()
    end_datetime = datetime.utcnow()
    try:
        start_datetime = datetime.fromisoformat(
            request.json['timeOfAllocation'])
        end_datetime = datetime.fromisoformat(
            request.json['taskToBePerformedBy'])
    except ValueError:
        abort(422, 'Please use the ISO datetime format. 2018-07-22T05:22:13.000Z')

    task = Task.query.get(task_id)
    task.asset_id = asset_id
    task.worker_id = end_datetime
    task.start_datetime = start_datetime
    task.end_datetime = end_datetime

    db.session.commit()
    return jsonify({'status': 'Success.'})


@bp.route('/get-tasks-for-worker/<int:worker_id>',
            methods=['GET'])
def get_tasks(worker_id):
    all_tasks = Worker.tasks.all()
    result = tasks.dump(all_tasks)
    return jsonify(result.data)
